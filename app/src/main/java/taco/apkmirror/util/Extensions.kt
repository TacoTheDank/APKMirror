package taco.apkmirror.util

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.getSystemService
import androidx.core.net.toUri
import taco.apkmirror.R

/**
 * Copies the text to the clipboard.
 *
 * @param label User-visible label for the clip data.
 * @param text  The actual text in the clip.
 */
fun Context.copyToClipboard(label: CharSequence?, text: CharSequence?) {
    // Gets a handle to the clipboard service.
    val clipboard = getSystemService<ClipboardManager>()
    if (clipboard != null) {
        // Creates a new text clip to put on the clipboard.
        val data = ClipData.newPlainText(label, text)
        clipboard.setPrimaryClip(data)
    } else {
        displayToast(R.string.clip_error)
    }
}

/**
 * Displays a toast to the viewer.
 *
 * @param string The text displayed in the toast.
 */
fun Context.displayToast(@StringRes string: Int) {
    Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
}

/**
 * Creates an intent action that will be viewed (a website, for example). Self-explanatory, really.
 *
 * @param uri The intent data URI.
 */
fun Activity.startIntent(uri: String) {
    val i = Intent(Intent.ACTION_VIEW, uri.toUri())
    startActivity(i)
}
